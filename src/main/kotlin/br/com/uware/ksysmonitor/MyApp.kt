package br.com.uware.ksysmonitor

import br.com.uware.ksysmonitor.view.MainView
import javafx.scene.Scene
import javafx.stage.Screen
import javafx.stage.Stage
import javafx.stage.StageStyle
import tornadofx.*
import tornadofx.Stylesheet.Companion.importServiceLoadedStylesheets


class MyApp() : App(MainView::class, Styles::class) {


    init {
        reloadStylesheetsOnFocus()
        reloadViewsOnFocus()
        importServiceLoadedStylesheets()
        importStylesheet(Styles::class)
    }

    override fun start(stage: Stage) {
        val screenMonitors = Screen.getScreens()
        var screenWidth = 0.0
        screenMonitors.map { screenWidth += it.bounds.width }


        with(stage) {
            initStyle(StageStyle.UTILITY)
            isAlwaysOnTop = false
            opacity = 0.0
            height = 0.0
            width = 0.0
            show()
        }
        val mainStage = Stage()
        with(mainStage) {
            initOwner(stage)
            scene = Scene(MainView().root)
            initStyle(StageStyle.TRANSPARENT)
            scene.fill = null
            FX.applyStylesheetsTo(scene)
            x = screenWidth - 320
            y = 50.0
            height = 300.0
            width = 300.0
            show()
        }


    }

}