package br.com.uware.ksysmonitor

import javafx.scene.paint.Color
import javafx.scene.paint.LinearGradient
import javafx.scene.text.FontWeight
import tornadofx.*

class Styles : Stylesheet() {
    companion object {
        val heading by cssclass()
        val gpublock by cssclass()
        val gpuinfocss by cssclass()
        val infocss by cssclass()
    }

    init {
        label and heading {
            padding = box(10.px)
            fontSize = 16.px
            textFill = Color.LIGHTBLUE
            fontWeight = FontWeight.BOLD
        }
        gpublock {
            //backgroundColor += Color.TRANSPARENT
        }
        infocss {
            padding = box(5.px)
            fontSize = 12.px
            textFill = Color.WHITE
            fontWeight = FontWeight.BOLD
        }
        gpuinfocss {
            padding = box(5.px)
            fontSize = 12.px
            textFill = Color.DARKBLUE
            fontWeight = FontWeight.BOLD
        }

    }
}