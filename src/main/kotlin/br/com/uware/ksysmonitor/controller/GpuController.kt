package br.com.uware.ksysmonitor.controller

import br.com.uware.ksysmonitor.core.GpuCore
import br.com.uware.ksysmonitor.model.Gpu
import javafx.application.Platform
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import tornadofx.Controller


/*
*   Author:     Rodrigo Leutz
*   Project:    tornadofx-gradle-project
*   Date:       11/11/2020
*/

class GpuController : Controller(){

    var gpu = GpuCore().getGpuInfo()
    private var gpuInfo = FXCollections.observableArrayList<Gpu>(
        gpu
    )

    val labelGpuName = SimpleStringProperty()
    val labelGpuMHz = SimpleStringProperty()
    val labelGpuMem = SimpleStringProperty()
    val labelGpuMemTotal = SimpleStringProperty()
    val labelGpuFan = SimpleStringProperty()
    val labelGpuTemp = SimpleStringProperty()
    val labelGpuPower = SimpleStringProperty()

    val progressMem = SimpleDoubleProperty()
    private var coreMaxSpeed: Double = 0.0
    val progressCore = SimpleDoubleProperty()
    private var tempMax = 0.0
    val progressTemp = SimpleDoubleProperty()
    private var fanMax = 2000.0
    val progressFan = SimpleDoubleProperty()
    private var powerMax = 100.0
    val progressPower = SimpleDoubleProperty()

    init {
        labelGpuName.value = gpuInfo[0].model
        labelGpuMemTotal.value = gpuInfo[0].memTotal.toString()
        coreMaxSpeed = gpuInfo[0].gpuClock.toDouble()
        tempMax = gpuInfo[0].tempMax.toDouble()
        progressCore.value = 1.0
        updateVars()
        runUpdates()
    }

    private fun runUpdates() {
        Platform.runLater {
            updateVars()
            Thread.sleep(500)
            runUpdates()
        }
    }
    fun updateVars() {
        gpu = GpuCore().getGpuInfo()
        gpuInfo = FXCollections.observableArrayList<Gpu>(
            gpu
        )
        labelGpuMHz.value = gpuInfo[0].gpuClock.toString()
        labelGpuMem.value = gpuInfo[0].memUse.toString()
        labelGpuTemp.value = gpuInfo[0].temp.toString()
        labelGpuFan.value = gpuInfo[0].fan.toString()
        labelGpuPower.value = gpuInfo[0].power.toString()

        //  Memory
        progressMem.value = (labelGpuMem.value.toDouble() / labelGpuMemTotal.value.toDouble()).toDouble()
        //  Core Speed
        if(coreMaxSpeed < labelGpuMHz.value.toDouble()) {
            coreMaxSpeed = labelGpuMHz.value.toDouble()
        }
        progressCore.value = labelGpuMHz.value.toDouble() / coreMaxSpeed
        //  Temp
        if(tempMax < labelGpuTemp.value.toDouble()){
            tempMax = labelGpuTemp.value.toDouble()
        }
        progressTemp.value = labelGpuTemp.value.toDouble() / tempMax
        //  Fan
        if(fanMax < labelGpuFan.value.toDouble()){
            fanMax = labelGpuFan.value.toDouble()
        }
        progressFan.value = labelGpuFan.value.toDouble() / fanMax
        //  Power
        if(powerMax < labelGpuPower.value.toDouble()){
            powerMax = labelGpuPower.value.toDouble()
        }
        progressPower.value = labelGpuPower.value.toDouble() / powerMax
    }


}