package br.com.uware.ksysmonitor.core


import br.com.uware.ksysmonitor.model.Cpu
import java.io.File


class CpuCore() {
    fun getCpuInfo(): Cpu {
        var cpu = Cpu()
        File("/proc/cpuinfo").forEachLine {
            if(it.contains("model")){
                cpu.model = it.split(":")[1].trim()
            }
            if(it.contains("MHz")){
                cpu.mhz.add(it.split(":")[1].trim().toDouble())
            }
        }
        for(x in 0..10) {
            if (File("/sys/class/hwmon/hwmon$x/name").readLines()[0] == "coretemp") {
                File("/sys/class/hwmon/hwmon$x/").walk().maxDepth(1).forEach {
                    if(it.toString().contains("temp") && it.toString().contains("_input") && !it.toString().contains("temp1_input")){
                        cpu.temp.add(it.readLines()[0].toInt()/1000)
                    }
                    if(it.toString().contains("temp1_input")){
                        cpu.packageTemp = it.readLines()[0].toInt()/1000
                    }
                }
                break
            }
        }
        return cpu
    }
    fun walkDirectoryRecursively(dirPath: String, pattern: Regex): Sequence<String> {
        val d = File(dirPath)
        require (d.exists() && d.isDirectory())
        return d.walk().map { it.name }.filter { it.matches(pattern) }.sorted().distinct() }
}