package br.com.uware.ksysmonitor.core

import br.com.uware.ksysmonitor.model.Gpu
import java.io.File
import java.util.*

class GpuCore () {
    fun getGpuInfo(): Gpu {
        var gpu = Gpu()
        val process = Runtime.getRuntime().exec("glxinfo -B")
        val sc = Scanner(process.inputStream)
        while(sc.hasNextLine()){
            val str = sc.nextLine()
            if(str.contains("Device:")){
                gpu.model = str.split("(")[0].split(":")[1].trim()
            }
        }
        sc.close()
        for(x in 0..10){
            if(File("/sys/class/hwmon/hwmon$x/name").readLines()[0] == "amdgpu"){
                gpu.gpuClock = File("/sys/class/hwmon/hwmon$x/freq1_input").readLines()[0].toInt()/1000000
                gpu.memClock = File("/sys/class/hwmon/hwmon$x/freq2_input").readLines()[0].toInt()/1000000
                gpu.memTotal = (File("/sys/class/hwmon/hwmon$x/device/mem_info_vram_total").readLines()[0].toLong()/1024/1024).toInt()
                gpu.memUse = (File("/sys/class/hwmon/hwmon$x/device/mem_info_vram_used").readLines()[0].toLong()/1024/1024).toInt()
                gpu.temp = File("/sys/class/hwmon/hwmon$x/temp1_input").readLines()[0].toInt()/1000
                gpu.tempMax = File("/sys/class/hwmon/hwmon$x/temp1_crit").readLines()[0].toInt()/1000
                gpu.fan = File("/sys/class/hwmon/hwmon$x/fan1_input").readLines()[0].toInt()
                gpu.power = File("/sys/class/hwmon/hwmon$x/power1_average").readLines()[0].toDouble()/1000000
                break
            }
        }
        return gpu
    }
}

