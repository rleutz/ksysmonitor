package br.com.uware.ksysmonitor

import tornadofx.*


fun main(args: Array<String>) {
    if(args.isEmpty() || args.size == 2) {
        if(args.isEmpty()) {
            launch<MyApp>()
        }
        else{
            launch<MyApp>(args[0], args[1])
        }
    }
}


