package br.com.uware.ksysmonitor.model

data class Cpu(
    var model: String = "",
    var packageTemp: Int = 0,
    var temp: ArrayList<Int> = ArrayList(),
    var mhz: ArrayList<Double> = ArrayList(),
    var mhzMax: ArrayList<Double> = ArrayList()
)