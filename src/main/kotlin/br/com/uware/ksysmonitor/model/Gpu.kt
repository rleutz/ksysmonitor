package br.com.uware.ksysmonitor.model

data class Gpu(
        var model: String? = "",
        var gpuClock: Int = 0,
        var memClock: Int = 0,
        var memUse: Int = 0,
        var memTotal: Int = 0,
        var temp: Int = 0,
        var tempMax: Int = 0,
        var fan: Int = 0,
        var power: Double = 0.0
)