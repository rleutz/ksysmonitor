package br.com.uware.ksysmonitor.playground

import br.com.uware.ksysmonitor.core.CpuCore
import javafx.stage.StageStyle




/*
*   Author:     Rodrigo Leutz
*   Project:    tornadofx-gradle-project
*   Date:       13/11/2020
*/

fun main() {
    val cpu = CpuCore().getCpuInfo()
    println(cpu)
}