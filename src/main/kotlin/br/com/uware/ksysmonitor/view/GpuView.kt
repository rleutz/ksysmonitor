package br.com.uware.ksysmonitor.view

import br.com.uware.ksysmonitor.Styles
import br.com.uware.ksysmonitor.controller.GpuController
import javafx.animation.Interpolator
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.control.ProgressBar
import javafx.scene.paint.Color
import javafx.util.Duration
import tornadofx.*

/*
*   Author:     Rodrigo Leutz
*   Project:    tornadofx-gradle-project
*   Date:       11/11/2020
*/

class GpuView : View("My View") {

    val gpuController: GpuController by inject()
    var oldValue: Double = 0.0

    override val root = vbox {
        alignment = Pos.CENTER
        background = null
        var gpuMhzOld: Double = gpuController.progressCore.value


//        val radeonImage = resources.url("icons/radeon.jpg")
        imageview("icons/radeon.png") {
            fitWidth = 100.0
            isPreserveRatio = true
            style {
                backgroundColor += Color.TRANSPARENT
            }
        }

        label(gpuController.labelGpuName) {
            addClass(Styles.heading)
        }

        hbox {
            alignment = Pos.CENTER_RIGHT
            label("GPU MHz:") {
                setStyles()
            }
            stackpane {
                progressbar(gpuController.progressCore) {
                    changeProgressColor()
                }
                hbox {
                    alignment = Pos.CENTER
                    label(gpuController.labelGpuMHz) {
                        addClass(Styles.gpuinfocss)
                    }
                    label("MHz") {
                        addClass(Styles.gpuinfocss)
                    }
                }

            }
        }

        hbox {
            alignment = Pos.CENTER_RIGHT
            label("GPU Mem:") {
                setStyles()
            }
            stackpane {
                alignment = Pos.CENTER
                progressbar(gpuController.progressMem) {
                    changeProgressColor()
                }
                hbox {
                    alignment = Pos.CENTER
                    label(gpuController.labelGpuMem) {
                        addClass(Styles.gpuinfocss)
                    }
                    label("/") {
                        addClass(Styles.gpuinfocss)
                    }
                    label(gpuController.labelGpuMemTotal) {
                        addClass(Styles.gpuinfocss)
                    }
                    label("MB") {
                        addClass(Styles.gpuinfocss)
                    }
                }

            }
        }

        hbox {
            alignment = Pos.CENTER_RIGHT
            label("GPU Temp:") {
                setStyles()
            }
            stackpane {
                progressbar(gpuController.progressTemp) {
                    changeProgressColor()
                }
                hbox {
                    alignment = Pos.CENTER
                    label(gpuController.labelGpuTemp) {
                        addClass(Styles.gpuinfocss)
                    }
                    label("°C") {
                        addClass(Styles.gpuinfocss)
                    }
                }
            }
        }
        hbox {
            alignment = Pos.CENTER_RIGHT
            label("GPU Fan:") {
                setStyles()
            }
            stackpane {
                progressbar(gpuController.progressFan) {
                    changeProgressColor()
                }
                hbox {
                    alignment = Pos.CENTER
                    label(gpuController.labelGpuFan) {
                        addClass(Styles.gpuinfocss)
                    }
                    label("RPM") {
                        addClass(Styles.gpuinfocss)
                    }
                }
            }
        }
        hbox {
            alignment = Pos.CENTER_RIGHT
            label("GPU Power:") {
                setStyles()
            }
            stackpane {
                progressbar(gpuController.progressPower){
                    changeProgressColor()
                }
                hbox {
                    alignment = Pos.CENTER
                    label(gpuController.labelGpuPower) {
                        addClass(Styles.gpuinfocss)
                    }
                    label("W") {
                        addClass(Styles.gpuinfocss)
                    }
                }

            }
        }
    }

    // Label Proprieties
    private fun Label.setStyles(){
        addClass(Styles.infocss)
        prefWidth = 100.0
    }

    // ProgressBar Proprieties
    private fun ProgressBar.changeProgressColor() {
        prefWidth = 200.0
        addClass(".progress-gpu")
        progressProperty().onChange {
            if(it > 0.9) {
                style {
                    accentColor = Color.RED
                }
            } else {
                style {
                    accentColor = Color.LIGHTBLUE
                }
            }
            progressProperty().value = oldValue
            timeline {
                keyframe(Duration.seconds(1.0)){
                    keyvalue(this@changeProgressColor.progressProperty(), it,
                        Interpolator.EASE_BOTH)
                }
            }
            oldValue = it
        }


    }

}
