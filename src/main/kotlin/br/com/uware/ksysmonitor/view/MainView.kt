package br.com.uware.ksysmonitor.view

import br.com.uware.ksysmonitor.Styles
import br.com.uware.ksysmonitor.controller.GpuController
import br.com.uware.ksysmonitor.controller.MainController
import javafx.geometry.Pos
import javafx.scene.paint.Color
import javafx.stage.StageStyle
import tornadofx.*

class MainView : View("KsysMonitor") {

    val gpuView: GpuView by inject()
    val cpuView: CpuView by inject()

    override val root = borderpane {
        style {
            backgroundColor += Color.TRANSPARENT
        }

        top = cpuView.root
        bottom = gpuView.root

    }

    override fun onDock() {
        currentStage?.scene?.fill = null
    }
}
